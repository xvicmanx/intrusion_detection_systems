"""
Script file to create the one vs all class to neural neural network misuse datasets.
Author: Victor Trejo
"""

import my_utils as mu
import os
import numpy as np

# Path to read the training and test datasets
currentDirectory = os.path.dirname(os.path.abspath(__file__))

dataPath = "{0}/../data/preprocessed/".format(currentDirectory)
# Reading training and test datasets
trainData = mu.readDataFile('{0}misuse.train'.format(dataPath))
testData = mu.readDataFile('{0}misuse.test'.format(dataPath))
trainRows, trainCols = trainData.shape
testRows, testCols = testData.shape

# Getting unique attack types 
attackClasses = list(set(trainData[:,trainCols-1]))
# Removing NORMAL label/type
attackClasses.remove('NORMAL')

numberClassesMap = [['NORMAL']]



print type(trainData)
resultTrain = np.zeros((trainRows, trainCols-1 + len(attackClasses)))
resultTrain[:,0:trainCols-1] = trainData[:,0:trainCols-1]
resultTrain = resultTrain.astype(str)
resultTest = np.zeros((testRows, testCols-1 + len(attackClasses)))
resultTest[:,0:testCols-1] = testData[:,0:testCols-1]
resultTest = resultTest.astype(str)

resultTest[:,-len(attackClasses):] = '0'
resultTrain[:,-len(attackClasses):] = '0'

for i in range(len(attackClasses)):
	attackClass = attackClasses[i]
	resultTrain[trainData[:,trainCols-1]==attackClass,-1-i] = '1'
	resultTest[testData[:,testCols-1]==attackClass,-1-i] = '1'
	numberClassesMap.append([attackClass])
	

mu.saveDataFile(np.asarray(resultTrain), dataPath,  "misuse_binary_output.train")
mu.saveDataFile(np.asarray(resultTest), dataPath,  "misuse_binary_output.test")
mu.saveDataFile(numberClassesMap, dataPath,  "number_classes_map.csv")