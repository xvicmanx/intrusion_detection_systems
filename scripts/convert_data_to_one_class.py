"""
Script file to create the one vs all class to neural neural network misuse datasets.
Author: Victor Trejo
"""

import my_utils as mu
import os

# Path to read the training and test datasets
currentDirectory = os.path.dirname(os.path.abspath(__file__))
inputDataPath = "{0}/../data/preprocessed/".format(currentDirectory)
dataPath = "{0}/../data/preprocessed/one_vs_all/".format(currentDirectory)
# Reading training and test datasets
trainData = mu.readDataFile('{0}misuse.train'.format(inputDataPath))
testData = mu.readDataFile('{0}misuse.test'.format(inputDataPath))
rows, cols = trainData.shape

# Getting unique attack types 
attackClasses = set(trainData[:,cols-1])
# Removing NORMAL label/type
attackClasses.remove('NORMAL')

# For each attack type two additional datasets will be created:
# one for training and other for testing:
# The class in the output fields will be 1 for the cases where
# the label was the attack type and 0 otherwise.
for attackClass in attackClasses:
	copyTrain = trainData.copy()

	# Saving relabeled train file
	copyTrain[copyTrain[:,cols-1]!=attackClass,cols-1] = 0
	copyTrain[copyTrain[:,cols-1]==attackClass,cols-1] = 1
	mu.saveDataFile(copyTrain, dataPath,  "misuse_{}.train".format(attackClass.lower()))

	# Saving relabeled test file
	copyTest = testData.copy()
	copyTest[copyTest[:,cols-1]!=attackClass,cols-1] = 0
	copyTest[copyTest[:,cols-1]==attackClass,cols-1] = 1
	mu.saveDataFile(copyTest, dataPath,  "misuse_{}.test".format(attackClass.lower()))