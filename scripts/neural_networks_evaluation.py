"""
This script tests a neural network model.
Author: Victor Trejo
"""
import os
import numpy as np
import decision_tree as dt 
import my_utils as mu
import time
import random
import neural_networks as nn
import pickle
import sys


def getClassLabel(data, cols, numberToClassMap):
	"""
	Based on the output get a more representative label.
	parameters: 
				data -> data value
				cols -> number of columns in the output value
				numberToClassMap -> dictionary to map number to class
	Returns: the label
	"""
	value = 0
	for i in range(cols):
		 if data[:,i] == 1:
		 	value =  i + 1
		 	break
	return numberToClassMap[value] if not numberToClassMap is None else value


def testAndPrintMetrics(dataInput, dataOutput, classes, model, caseName, numberToClassMap):
	"""
	Tests and print evaluation metrics.
	Parameters:
				dataInput  -> test features data to evaluate model
				dataOutput -> test  labels data to evaluate model
				classes    -> possibles classes that can be predicted
				model      -> trained model to be tested
				caseName   -> label of the model Category being tested (anomaly, misuse)
				numberToClassMap -> to map numbers to string labels
	"""
	# Data dimensions
	rows, cols = dataInput.shape
	outputRows, outputCols = dataOutput.shape
	# Confusion Matrix Data Structure
	confusionMatrix = {pc:{rc:0 for rc in classes} for pc in classes}
	# To store the true positives by class
	truePositives = {c:0 for c in classes}
	# To store  the false positives by class
	falsePositives = {c:0 for c in classes}
	# To store  the false negatives by class
	falseNegatives = {c:0 for c in classes}
	# To keep track of the number of correctly predicted samples overall
	correctlyPredicted = 0

	# For every row/sample in the data:
	# predict and evaluate.
	for i in range(rows):
		# Saving the real label of the data
		real = np.round(dataOutput[i,:]).astype(int)
		real = getClassLabel(real, outputCols, numberToClassMap)
		# predicting the label
		predicted = np.round(model.predict(dataInput[i,:])).astype(int)
		predicted = getClassLabel(predicted, outputCols, numberToClassMap)
		# Saving the prediction outcome in the confusion matrix
		confusionMatrix[predicted][real]+=1
		# If the label was correctly predicted
		# then increase the number of correctly predicted label and
		# the number of true positives for the predicted class.
		if predicted == real:
			truePositives[predicted]+=1
			correctlyPredicted+=1
		# Otherwise, increase
		# the number of false positives for the predicted class.
		# the number of false negatives for the real class.
		else:
			falsePositives[predicted]+=1
			falseNegatives[real]+=1

	# Printing the evaluation metrics out on the screen.
	mu.printEvaluationMetrics( \
		classes, caseName, \
		100*(float(correctlyPredicted)/rows), \
		confusionMatrix, truePositives, \
		falsePositives, falseNegatives \
	)
	


def testModel(model, testDataInput, testDataOutput, caseName, numberToClassMap):
	"""
	Tests the classifier Model.
	Parameters: 
				model    -> classifier model to test.
				testDataInput -> Features data used to test the model.
				testDataOutput -> Labels data used to test the model.
				caseName -> Model Category Being tested (e.g. anomaly, misuse) special case
				numberToClassMap -> to map numbers to string labels
	"""
	# Finding the  all the possible class labels
	outputRows, outputCols = testDataOutput.shape
	classes = set(range(0, outputCols + 1))
	if not numberToClassMap is None:
		classes = set(numberToClassMap)

	# Testing model and calculating its statistics metrics
	testAndPrintMetrics(testDataInput, testDataOutput, classes,  model, caseName, numberToClassMap)


def buildModelAndTest(dataInput, dataOutput, trainedFile, caseName, numberToClassMap):
	"""
	With the trained parameters stored in the 'trainedFile', builds 
	a neural network model and test it.
	Parameters:
				dataInput ->  test data features to evaluate model.
				dataOutput ->  test data labels to evaluate model.
				trainedFile -> file that contains trained parameters for the model.
				caseName -> Model Category Being tested (e.g. anomaly, misuse) special case
				numberToClassMap -> to map numbers to string labels
	"""
	activation = nn.SigmoidActivation()
	# reading trained parameters
	data = pickle.load(trainedFile)
	weightsByLayers, thresholdsByLayers = data['weights'], data['thresholds']
	# building model
	multiLevelPercetron = nn.NeuralNetwork(weightsByLayers, thresholdsByLayers, activation)
	# evaluating model
	testModel(multiLevelPercetron, dataInput, dataOutput, caseName, numberToClassMap)



# Program starting point
if __name__ == "__main__":
	if len(sys.argv) < 5: 
		print "Invalid number of arguments!"
		print "Please provide 'data File Path' 'trained file path' 'number of cols in the output' 'case name'"
	else: 
		# Reading the file names from the parameters passed from the console
		dataFilePath = sys.argv[1]
		trainedFilePath = sys.argv[2]
		outputCols = int(sys.argv[3])
		caseName = sys.argv[4]

		# To map number classes to label strings
		numberToClassMap = None
		if len(sys.argv) > 5:
			numberToClassMap =  mu.readDataFile(sys.argv[5], 0).tolist()
			
		

		# data file
		data =  mu.readDataFile(dataFilePath, 1)
		data = np.matrix(data, dtype=np.float64)
		rows, cols = data.shape
		dataInput, dataOutput = data[:,0:cols-outputCols], data[:,-outputCols:]
		trainedFile = open(trainedFilePath, 'rb')

		print "Testing Started\n"
		mu.measureRunningTime(lambda: buildModelAndTest(\
			dataInput,
			dataOutput,
			trainedFile,
			caseName,
			numberToClassMap
			)
		)

		print "The memory usage was: {} MB\n".format(mu.getMemoryUsage())
		print "Testing Finished\n"
		
	

