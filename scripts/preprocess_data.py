"""
Author: Victor Trejo
Description: This is the script used to preprocess the IDS attacks data.
"""

# importing necessary libraries
import os
import numpy as np
import random
import copy
import my_utils as mu

# Program starting point
if __name__ == "__main__":
	print "PREPROCESSING DATA..."
	
	# To store misuse training and test data
	misuseData = []
	misuseTrainingData = []
	misuseTestData = []
	# To store anomaly training and test data
	anomalyData = []
	anomalyTrainingData = []
	anomalyTestData = []


	# current directory
	currentDirectory = os.path.dirname(os.path.abspath(__file__))
	# Unprocessed data directory
	attacksDataPath = "{0}/../data/optimized_attacks_normal/".format(currentDirectory)
	# Processed data directory
	preprocessedDataPath = "{0}/../data/preprocessed/".format(currentDirectory)

	# process attack files
	for attackFileName in os.listdir(attacksDataPath):
		# attack type label
		attackType = attackFileName.replace("Optimized_", "").upper()
		# attack file
		attackFile = open("{0}{1}".format(attacksDataPath, attackFileName), 'r')

		try:
			# Reading the data for a given attack
			fileData = np.loadtxt(attackFile, delimiter=', ')	
			# Removing outliers
			fileData =  mu.removeDataOutliers(fileData, 2)
			# data set dimensions
			rows, cols = fileData.shape
			# number of samples to take
			# Cause there are too many normal and Neptune samples
			# we are taking more than the others  
			ToTake = 100
			if attackType == 'NORMAL': ToTake = 2000
			if attackType == 'NEPTUNE': ToTake = 600
												
			# Only taking attacks that have enough data to train and test.
			if rows > 100:
				# Converting data set to list of lists and shuffles its rows
				dataList = fileData.astype('str').tolist()
				random.shuffle(dataList)
				
				# adding data
				for i in range(ToTake):
					# taking a sample
					misuseSample = list(dataList.pop())
					# copying that sample
					anomalySample = copy.deepcopy(misuseSample)
					# For the case of misuse add the label of the attack type or normal
					misuseSample.append(attackType)
					# For the case of anomaly add the labels NORMAL or NOT_NORMAL
					anomalySample.append('NORMAL' if attackType == 'NORMAL' else 'NOT_NORMAL')
					# append the sampes to its respective data set.
					misuseData.append(misuseSample)
					anomalyData.append(anomalySample)
				
				print "File {0} contains {1} rows".format(attackFileName, rows)

		except Exception, e:
			print "Error {0} in file {1}".format(e, attackFileName)


	# Calculating the number of samples that will be taken by each data set.
	samplesToTrainingDataset = int(len(misuseData) * (float(2)/3))
	samplesToTestDataset = len(misuseData) - samplesToTrainingDataset

	random.shuffle(misuseData)
	random.shuffle(anomalyData)	

	# adding training data
	for i in range(samplesToTrainingDataset):
		misuseTrainingData.append(misuseData.pop())
		anomalyTrainingData.append(anomalyData.pop())

	# Adding test data
	for i in range(samplesToTestDataset):
		misuseTestData.append(misuseData.pop())
		anomalyTestData.append(anomalyData.pop())

	# Saving Preprocessed Data Files	
	print "SAVING PREPROCESSED DATA FILES"
	mu.saveDataFile(misuseTrainingData, preprocessedDataPath,  "misuse.train")
	mu.saveDataFile(misuseTestData, preprocessedDataPath,  "misuse.test")
	mu.saveDataFile(anomalyTrainingData, preprocessedDataPath,  "anomaly.train")
	mu.saveDataFile(anomalyTestData, preprocessedDataPath,  "anomaly.test")
	print "PREPROCESSING DONE" 