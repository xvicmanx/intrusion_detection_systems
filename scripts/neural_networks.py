"""
Neural network implementation module.
Author: Victor Trejo
"""
# importing necessary libraries
import random
from math import exp
from math import sqrt
import numpy as np

# For printing the sum of squared errors by iteration
PRINT_SQUARED_ERRORS = False


def getInitialWeightsAndThresholdsByLayers(neuronsByLayers, numberOfInputs):
	"""
	Gets the initial values  for the weights and thresholds for all the layers
	of the neural network
	Parameters: 
				neuronsByLayers -> a tuple that specified the number of neurons by layer
				numberOfInputs ->  number of inputs of the neural network
	Returns: the initialized weights and thresholds by layer
	"""

	numberOfLayers = len(neuronsByLayers)
	# to store the weights of the neurons by layer
	weightsByLayers = []
	# to store the thresholds of the neurons by layer
	thresholdsByLayers = []
	for l in range(numberOfLayers):
		# For all the neurons in the current layer initiliaze the values 
		# of weights and thresholds.
		numberOfNeurons = neuronsByLayers[l]	
		weights = np.ones((numberOfNeurons, numberOfInputs), dtype=np.float64)
		thresholds = np.ones(numberOfNeurons, dtype=np.float64)

		for n in range(numberOfNeurons):
			weights[n, :] =  getInitialWeights(numberOfInputs)
			thresholds[n] = getInitialValue(numberOfInputs)

		weightsByLayers.append(weights)
		thresholdsByLayers.append(thresholds)
		numberOfInputs = numberOfNeurons
	return weightsByLayers, thresholdsByLayers


def getInitialWeightsAndThresholdsCorrectionsByLayers(\
	weightsByLayers,
	thresholdsByLayers
):
	"""
	Gets the initial values  for the weights and thresholds correction for all the layers
	of the neural network
	Parameters: 
				weightsByLayers -> the data structure that store the weights by layers
				thresholdsByLayers ->  the data structure that store the thresholds by layers
	Returns: the initialized weights and thresholds correction by layer (initially zeros values)
	"""
	# to store the weights correction values 
	# to be used when back propagating 
	weightCorrectionsByLayer = []
	# to store the thresholds correction values 
	# to be used when back propagating 
	thresholdsCorrectionsByLayer = []

	numberOfLayers = len(weightsByLayers)
	for l in range(numberOfLayers):
		# Initialize the values for the weights
		# and thresholds corrections of the current layer.
		weightCorrectionsByLayer.append(\
			np.zeros(weightsByLayers[l].shape, dtype=np.float64)
		)
		
		thresholdsCorrectionsByLayer.append(\
			np.zeros(thresholdsByLayers[l].shape, dtype=np.float64)
		)
	return weightCorrectionsByLayer, thresholdsCorrectionsByLayer


def getNewLearningRate(learningRate, previousError, currentError):
	"""
	Determines the new value of the learning rate based on the values of the
	sum squared errors of the previous and current epoch
	Parameters:
				learningRate  -> current learning rate value
				previousError -> sum of squared errors of previous epoch
				currentError  -> sum of squared errors of current epoch
	Returns: the new value of the learning rate
	"""		
	ratio = currentError/previousError
	if ratio > 1.04: return 0.7 * learningRate
	elif previousError >= currentError: return 1.05 * learningRate
	else: return learningRate
			


def getSumSquaredErrors(\
	dataInput,
	dataOutput,
	activation,
	weightsByLayers,
	thresholdsByLayers):
	"""
	Calculates the sum squared errors of the output
	after training a neural network using the weights and thresholds given.
	Parameters:
				dataInput          -> features of the data
				dataOutput         -> output values of the data
				activation         -> activation function
				weightsByLayers    -> weights by layers
				thresholdsByLayers -> thresholds by layers
	Returns: the calculated sum squared error
	"""
	rows, cols = dataInput.shape
	weightsByLayers, thresholdsByLayers
	sumSquaredError = 0.0
	for r in range(rows):
		# With the current input, thresholds values and weights
			# calculates the neurons outputs by layer.
			inputValue = dataInput[r, :]
			outputByLayers = predictOutputByLayer(\
				inputValue,
				activation,
				thresholdsByLayers,
				weightsByLayers
			)
			# calculate the error between the calculated output and the real output.
			calculatedOutput = outputByLayers[len(outputByLayers)-1]
			error  = dataOutput[r,:] - calculatedOutput
			sumSquaredError = sumSquaredError +  np.sum(np.square(error))
	return sumSquaredError


def train(\
	dataInput, dataOutput, activation,
	learningRate,
	neuronsByLayers,
	momentumConstant = 0.95,
	
):
	"""
	Finds the weights and thresholds values for a multilayer neural network by
	applying back and forward propagation algorithm.
	Parameters:
				x -> input data
				y -> output data
				learningRate ->  learning rate of the algorithm
				neuronsByLayers -> is a tuple that indicates the number of neurons to be used 
								   by layer
				
				momentumConstant -> is value between 0 and 1 that is used to accelerate 
									the convergence time of the algorithm. (default is 0.95)

	Returns: the learned weights and thresholds values for all the layers.
	"""
	totalRows, totalCols = dataInput.shape

	totalSamplesTraining = int((float(5)/6) * totalRows)
	#For Validation and early stop functionality
	# Only taking 1/6 of the data
	validationDataInput, validationDataOutput = dataInput[totalSamplesTraining:,:], dataOutput[totalSamplesTraining:,:]
	# Only taking the 5/6 of the data
	dataInput, dataOutput = dataInput[:totalSamplesTraining,:], dataOutput[:totalSamplesTraining,:]

	rows, cols = dataInput.shape

	# to store errors by epoch
	sumSquaredErrorsStore = []
	# to store the learning rate by epoch
	learningRateValues = []
	# activation function to be applied to the weighted 
	# sum of the neurons inputs
	calculateValue = np.vectorize(activation.calculate)
	
	numberOfLayers = len(neuronsByLayers)
	
	# to store the weights and thresholds of the neurons by layer, respectively
	weightsByLayers, thresholdsByLayers = getInitialWeightsAndThresholdsByLayers(\
		neuronsByLayers,
		cols
	)

	# Initializing weights and thresholds corrections by layers
	WCBL, TCBL = getInitialWeightsAndThresholdsCorrectionsByLayers(\
		weightsByLayers,
		thresholdsByLayers
    )

	# to store the weights correction values 
	# to be used when back propagating 
	weightCorrectionsByLayer = WCBL
	# to store the thresholds correction values 
	# to be used when back propagating 
	thresholdsCorrectionsByLayer = TCBL

	# Forward and back propagation until the sum of squared errors is small enough
	sumSquaredError = float("inf")
	# To keep track error each five epochs
	lastValidationSumSquaredError = float("inf")
	# To keep track of the best weights and thresholds each five epochs
	lastWeightsByLayer, lastTresholdsByLayers = weightsByLayers, thresholdsByLayers

	while(sumSquaredError > 0.001):
		sumSquaredError = 0.0
		# for each input sample:
		# calculates the output, then calculates
		# the error of this output with the corrected output
		# continuing calculating this errors for the neurons
		# of the previous layers until reaching the input layer
		# and consequently modifying the weights and thresholds of this values
		# in order to reduce the error.
		for r in range(rows):
			# With the current input, thresholds values and weights
			# calculates the neurons outputs by layer.
			inputValue = dataInput[r, :]
			outputByLayers = predictOutputByLayer(\
				inputValue,
				activation,
				thresholdsByLayers,
				weightsByLayers
			)
			# calculate the error between the calculated output and the real output.
			calculatedOutput = outputByLayers[len(outputByLayers)-1]
			error  = dataOutput[r,:] - calculatedOutput
			sumSquaredError = sumSquaredError + np.sum(np.square(error))

			# Then calculate the error gradient in the output layer
			errorGradient = outputErrorGradient(calculatedOutput, error)
			# Continue propagating this error by calculating also the error gradient
			# of in the previous layers until reaching the input layer
			for l in range(numberOfLayers-1, 0, -1):
				
				# calculates the weight corrections of the weights for the input 
				# of the current layer
				weightCorrections = calculateWeightCorrections(\
					learningRate,
					outputByLayers[l-1],
					errorGradient
				)
				
				# calculates the thresholds corrections of the thresholds for the input 
				# of the current layer
				thresholdsCorrections = calculateThresholdCorrections(\
					learningRate,
					errorGradient
				)

				# updating weights and thresholds corrections, respectively
				weightCorrectionsByLayer[l] = momentumConstant * weightCorrectionsByLayer[l] +  weightCorrections
				thresholdsCorrectionsByLayer[l] = thresholdsCorrections
				
				# calculating the error gradient of the neurons' outputs of the previous layer.
				errorGradient = outputErrorGradient(\
					outputByLayers[l-1],
					errorGradient*weightsByLayers[l]
				)

			# Calculating the weights correction of the inputs in the neurons of the input layer
			weightCorrections = calculateWeightCorrections(\
				learningRate,
				inputValue,
				errorGradient
			)
			# Calculating the thresholds correction of in the neurons of the input layer
			thresholdsCorrections = calculateThresholdCorrections(\
				learningRate,
				errorGradient
			)

			# updating weights and thresholds corrections, respectively
			weightCorrectionsByLayer[0] = momentumConstant * weightCorrectionsByLayer[0] + weightCorrections
			thresholdsCorrectionsByLayer[0] = thresholdsCorrections
			
			# Once all the weights and thresholds corrections have been calculated,
			# proceed updating the weights and thresholds by layer.
			for l in range(numberOfLayers):
				weightsByLayers[l] = weightsByLayers[l] + weightCorrectionsByLayer[l]
				thresholdsByLayers[l] = thresholdsByLayers[l] + thresholdsCorrectionsByLayer[l]


		errorValue = sumSquaredError
		epochs = len(sumSquaredErrorsStore)

		# Every Five epochs we are checking the error against the validation set
		if epochs % 5 == 0:
			# Calculationg sum squared errors in the validation set
			currentValidationError = getSumSquaredErrors(\
				validationDataInput,
				validationDataOutput,
				activation,
				weightsByLayers,
				thresholdsByLayers
			)

			# If the validation error is less than the one five epochs before
			# its an early stop.
			if (currentValidationError >= lastValidationSumSquaredError).all():
				# For debugging purposes
				if PRINT_SQUARED_ERRORS: print "\nThere was an Early Stop!\n"
				# If there is an early stop the algorithm, return those  learned weights and thresholds
				# by layer.
				return lastWeightsByLayer, lastTresholdsByLayers, sumSquaredErrorsStore, learningRateValues
			else:
				lastValidationSumSquaredError = currentValidationError
				lastWeightsByLayer, lastTresholdsByLayers = weightsByLayers, thresholdsByLayers
				if PRINT_SQUARED_ERRORS: 
					print "\nSum Squared Error in Validation Set = {}\n".format(lastValidationSumSquaredError)
				

		# For debugging purposes
		if PRINT_SQUARED_ERRORS: 
			print "Value of the error at epoch {0} : {1}".format(epochs, errorValue)

		# Dynamic learning rate functionality: When error is increasing
		# decreases learning rate, and increase when it is decreasing
		if epochs > 0:
			learningRate = getNewLearningRate(\
				learningRate,
				sumSquaredErrorsStore[epochs - 1], 
				errorValue
			)

		sumSquaredErrorsStore.append(errorValue)
		learningRateValues.append(learningRate)
	# Once the algorithm has converged, return those  learned weights and thresholds
	# by layer.
	return weightsByLayers, thresholdsByLayers, sumSquaredErrorsStore, learningRateValues


def outputErrorGradient(output, error):
	"""
	Calculates the gradient of a neurons outputs.
	Parameters:
				output -> outputs of the neurons
				error  -> error of the outputs.
	Returns: the calculated output gradient. 
	"""
	result = np.multiply(output, (1-output))
	return np.multiply(result, error)

def calculateWeightCorrections(learningRate, dataInput, errorGradient):
	"""
	Calculates the corrections that should given to the weights of the 
	neurons of a layer.
	Parameters:
				learningRate  -> learning rate of the neuron
				dataInput 	  -> input values in the layer's neurons.
				errorGradient -> error gradient's of the output
	Returns: the calculated weights corrections to the neurons' weights
	"""
	return learningRate*(dataInput.T * errorGradient).T

def calculateThresholdCorrections(learningRate, errorGradient):
	"""
	Calculates the corrections that should given to the thresholds to the 
	neurons of a layer.
	Parameters:
				learningRate  -> learning rate of the neuron
				errorGradient -> error gradient's of the output
	Returns: the calculated thresholds corrections to the neurons' thresholds
	"""
	return -learningRate*errorGradient


def getInitialValue(numberOfInputs):
	"""
	Generates a random value for a neuron weight or threshold 
	between -1.0/sqrt(numberOfInputs) and 1.0/sqrt(numberOfInputs).
	Parameters: 
				numberOfInputs -> number of inputs of neuron
	Returns: the random value between range.
	"""
	return random.uniform(-1.0/sqrt(numberOfInputs), 1.0/sqrt(numberOfInputs))

def getInitialWeights(numberOfInputs):
	"""
	Initializes values the weights of the inputs for the neuron of a
	layer.
	Parameters:
				numberOfInputs -> number of inputs of neuron
	Returns: the initialed weights values.
	"""
	weights = np.ones(numberOfInputs, dtype=np.float64)
	for i in range(numberOfInputs):
		weights[i] = getInitialValue(numberOfInputs)
	return weights



def predictValue(\
	dataInput,
	activation,
	thresholdsByLayers,
	weightsByLayers):
	"""
	Given a data input, and activation function,
	the thresholds and weights values by layer,
	calculates neurons output values of the neurons in
	the output layer.
	Parameters: 
				dataInput -> data input to the neural Network
				activation -> activation function
				thresholdsByLayers -> thresholds values by layer
				weightsByLayers -> weights values by layer
	Returns: the output values of the neurons in the output layer.
	"""
	values = predictOutputByLayer(dataInput, activation, thresholdsByLayers, weightsByLayers)
	return values[len(values)-1]


def predictOutputByLayer(\
	dataInput,
	activation,
	thresholdsByLayers,
	weightsByLayers):
	"""
	Given a data input, and activation function,
	the thresholds and weights values by layer,
	calculates neurons output values by layer.
	Parameters: 
				dataInput -> data input to the neural Network
				activation -> activation function
				thresholdsByLayers -> thresholds values by layer
				weightsByLayers -> weights values by layer
	Returns: the predicted neurons output values by layer.
	"""
	calculate = np.vectorize(activation.calculate)
	inputValue = dataInput
	calculatedOuptut = None
	numberOfLayers = len(thresholdsByLayers)
	outputByLayers = []
	# layer by layer starting from the input layer
	# calculates the output values of the neurons 
	# until reaching the output layer
	for l in range(numberOfLayers):
		# Weights of the neurons of the current layer
		weights = weightsByLayers[l]
		# thresholds values of the neurons of the current layer
		thresholds = thresholdsByLayers[l]
		# applying activation function to the result of the weighted
		# sum of the neuron layer minus the thresholds values of the 
		# neurons.
		calculatedOuptut =  calculate(inputValue * weights.T - thresholds)
		inputValue = calculatedOuptut
		outputByLayers.append(calculatedOuptut)
	return outputByLayers






class NeuralNetwork(object):
	"""Neural Network predictor model representation"""
	def __init__(\
		self,
		weightsByLayers,
		thresholdsByLayers,
		activation):
		super(NeuralNetwork, self).__init__()
		# training weights by layers
		self.__weightsByLayers = weightsByLayers
		# training thresholds by layers
		self.__thresholdsByLayers = thresholdsByLayers
		# activiation function
		self.__activation = activation
		
	def predict(self, dataInput):
		"""
		Given a data input predicts its output value.
		Parameters:
					dataInput ->  input to predicts its outputs value.
		Returns: the predicted values
		"""
		return predictValue(\
			dataInput,
			self.__activation,
			self.__thresholdsByLayers,
			self.__weightsByLayers
		)




# Neural Network activation functions

class NeuralNetworkActivation(object):
	"""Neural Network Activation function"""
	def __init__(self):
		super(NeuralNetworkActivation, self).__init__()

	def calculate(self, value):
		raise Exception('Should implement this method!')	

class SignActivation(NeuralNetworkActivation):
	"""Sign Activation function"""
	def __init__(self, threshold):
		super(SignActivation, self).__init__()
		self.__threshold = threshold

	def calculate(self, value):
		return 1.0 if value >= self.__threshold else -1.0 

class StepActivation(NeuralNetworkActivation):
	"""Step Activation function"""
	def __init__(self, threshold = 0.0):
		super(StepActivation, self).__init__()
		self.__threshold = threshold

	def calculate(self, value):
		greaterOrEqual = np.greater(value, self.__threshold)  or\
						 np.isclose(value, self.__threshold)
		return 1.0 if greaterOrEqual else 0.0

class SigmoidActivation(NeuralNetworkActivation):
	"""Sigmoid Activation function"""
	def __init__(self):
		super(SigmoidActivation, self).__init__()

	def calculate(self, value):
		return np.float64(1.0)/(1 + np.exp(-value))

class HyperbolicTangentActivation(NeuralNetworkActivation):
	"""Hyperbolic Tangent Activation function"""
	def __init__(self, a = 1.716, b = 0.667):
		super(HyperbolicTangentActivation, self).__init__()
		self.__a = a
		self.__b = b

	def calculate(self, value):
		numerator = (np.float64(2.0) * self.__a)
		exponent = np.float64(-self.__b*value)
		denominator = (1 + exp(exponent))
		return (numerator/denominator) - self.__a

	
class LinearActivation(NeuralNetworkActivation):
	"""Linear Activation function"""
	def __init__(self):
		super(LinearActivation, self).__init__()

	def calculate(self, value):
		return value
