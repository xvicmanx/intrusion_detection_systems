"""
This script trains and tests the Intrusion Detection Systems (anomaly, misuse).
Author: Victor Trejo
"""
import os
import numpy as np
import decision_tree as dt 
import my_utils as mu
import time

def measureRunningTime(operation):
	"""
	Measures the time that takes an operation
	"""
	start = time.time()
	operation()
	end = time.time()
	print  "Took {0} seconds.\n\n".format(end - start)

def testAndPrintMetrics(data, classes, model, caseName):
	"""
	Tests and print evaluation metrics.
	Parameters:
				data     -> test data to evaluate model
				classes  -> possibles classes that can be predicted
				model    -> trained model to be tested
				caseName -> label of the model Category being tested (anomaly, misuse)
	"""
	# Data dimensions
	rows, cols = data.shape
	# Confusion Matrix Data Structure
	confusionMatrix = {pc:{rc:0 for rc in classes} for pc in classes}
	# To store the true positives by class
	truePositives = {c:0 for c in classes}
	# To store  the false positives by class
	falsePositives = {c:0 for c in classes}
	# To store  the false negatives by class
	falseNegatives = {c:0 for c in classes}
	# To keep track of the number of correctly predicted samples overall
	correctlyPredicted = 0

	# For every row/sample in the data:
	# predict and evaluate.
	for row in data:
		# Saving the real label of the data
		real = row[cols-1]
		# predicting the label
		predicted = model.predict(row)
		# Saving the prediction outcome in the confusion matrix
		confusionMatrix[predicted][real]+=1
		# If the label was correctly predicted
		# then increase the number of correctly predicted label and
		# the number of true positives for the predicted class.
		if predicted == real:
			truePositives[predicted]+=1
			correctlyPredicted+=1
		# Otherwise, increase
		# the number of false positives for the predicted class.
		# the number of false negatives for the real class.
		else:
			falsePositives[predicted]+=1
			falseNegatives[real]+=1

	# Printing the evaluation metrics out on the screen.
	mu.printEvaluationMetrics( \
		classes, caseName, \
		100*(float(correctlyPredicted)/rows), \
		confusionMatrix, truePositives, \
		falsePositives, falseNegatives \
	)
	


def getAttributeValues(data, dataTypes):
	"""
	Gets the domain of the attributes. (Possible values they can take)
	Parameters: 
				data 	  ->   It's the data set
				dataTypes ->   The types of the attributes (Continuous or Categorical)
	Returns: a dictionary with attribute:values pair.
	"""
	# Reading dataset dimensions
	rows, cols = data.shape
	# To store the attributes domains
	attributeValues = {}
	# Finding domain for each attribute
	for attribute in range(cols-1):
		# If the attribute is continuous it eventually be converted by the 
		# decision tree training algorithm 
		# (0 values less than threshold, 1 values greater than threshold)
		areContinuous = dataTypes[attribute] == 'Continuous'
		# That's why the domain will contain 0 or 1
		values = set(['0', '1']) if areContinuous else set(data[:,attribute])
		attributeValues[attribute] = values
	return attributeValues	 


def trainModel(trainData, dataTypes, attributeValues):
	"""
	Trains the classifier Model.
	Parameters: 
				trainData       -> Training data to train the model
				dataTypes       -> The types of the attributes (Continuous or Categorical)
				attributeValues -> Possible Values the attributes can take
	Returns: A Decision Tree model.
	"""
	return dt.buildDecisionTree(trainData, dataTypes, attributeValues)


def testModel(model, testData, caseName):
	"""
	Tests the classifier Model.
	Parameters: 
				model    -> classifier model to test.
				testData -> Data used to test the model.
				caseName -> Model Category Being tested (e.g. anomaly, misuse)
	"""
	rows, cols = testData.shape
	# Finding the  all the possible class labels
	classes = set(testData[:, cols-1])
	# Testing model and calculating its statistics metrics
	testAndPrintMetrics(testData, classes,  model, caseName)


def readDataFile(directory, fileName, skipHeader = 0):
	"""
	Reads a data file at a given location.
	Parameters:
				directory  -> directory of the file.
				fileName   -> Name of the file.
				skipHeader -> How many lines from header to skip 
	Returns: the read data file (as a matrix)
	"""
	return np.genfromtxt("{0}{1}".format(directory, fileName), delimiter=",", dtype='str', skip_header = skipHeader)
	

def testIntrusionDetectionSystem(iDType):
	"""
	Tests and evaluate a Intrusion Detection System
	Parameters:
				iDType -> type of IDS (either anomaly or misuse)
	"""
	# Path to read the training and test datasets
	currentDirectory = os.path.dirname(os.path.abspath(__file__))
	dataPath = "{0}/../data/preprocessed/".format(currentDirectory)
	# Reading training and test datasets
	print "Training {0} System...".format(iDType)
	trainData = readDataFile(dataPath, '{0}.train'.format(iDType))
	print "Testing {0} System...".format(iDType)
	testData = readDataFile(dataPath, '{0}.test'.format(iDType))
	# Reading attributes types
	rows, cols = testData.shape
	dataTypesDataSet = readDataFile(dataPath, "../data_fields.csv", 1)
	dataTypes = []
	for row in dataTypesDataSet: dataTypes.append(row[2].title())
	# dataTypes = ["Continuous"] * (cols-1)
	# Getting attribute's domains
	attributeValues = getAttributeValues(np.concatenate((trainData, testData), axis=0), dataTypes)
	# Training the model using the training data
	model = trainModel(trainData, dataTypes, attributeValues)
	# Testing model and printing statistics metrics
	testModel(model, testData, iDType.upper())


# Program starting point
if __name__ == "__main__":
	# Testing and evaluating both of the Intrusion Detection Systems
	measureRunningTime(lambda: testIntrusionDetectionSystem('misuse'))
	measureRunningTime(lambda: testIntrusionDetectionSystem('anomaly'))
	