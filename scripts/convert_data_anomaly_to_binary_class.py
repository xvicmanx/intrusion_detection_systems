"""
Script file to create the one vs all class to neural neural network anomaly datasets.
Author: Victor Trejo
"""

import my_utils as mu
import os

# Path to read the training and test datasets
currentDirectory = os.path.dirname(os.path.abspath(__file__))
dataPath = "{0}/../data/preprocessed/".format(currentDirectory)

# Reading training and test datasets
trainData = mu.readDataFile('{0}anomaly.train'.format(dataPath))
testData = mu.readDataFile('{0}anomaly.test'.format(dataPath))
rows, cols = trainData.shape

# Saving relabeled train file
trainData[trainData[:,cols-1]!="NORMAL",cols-1] = 0
trainData[trainData[:,cols-1]=="NORMAL",cols-1] = 1
mu.saveDataFile(trainData, dataPath,  "anomaly_binary_output.train")

# Saving relabeled test file
testData[testData[:,cols-1]!="NORMAL",cols-1] = 0
testData[testData[:,cols-1]=="NORMAL",cols-1] = 1
mu.saveDataFile(testData, dataPath, "anomaly_binary_output.test")