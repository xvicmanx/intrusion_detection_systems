"""
Neural network training algorithm parts' unit tests.
Author: Victor Trejo
"""

import unittest
import neural_networks as nn
import numpy as np

class NeuralNetworkTestCase(unittest.TestCase):

  def matrix(self, values):
    return np.matrix(values, dtype=np.float64)

  def test_predict_value_function(self):
    dataInput = np.matrix([1, 1], dtype=np.float64)
    activation = nn.SigmoidActivation()
    thresholdsByLayers = []
    weightsByLayers = []
    weightsByLayers.append(self.matrix([[0.5, 0.4], [0.9, 1.0]]))
    weightsByLayers.append(self.matrix([[-1.2, 1.1]]))

    thresholdsByLayers.append(self.matrix([0.8, -0.1]))
    thresholdsByLayers.append(self.matrix([0.3]))

    output = nn.predictValue(dataInput, activation, thresholdsByLayers, weightsByLayers)
    desiredOutput = self.matrix([[ 0.50972421]])
    self.assertTrue(np.isclose(output, desiredOutput))

  def test_error_gradient(self):
    calculatedOutput = self.matrix([[ 0.50972421]])
    output = self.matrix([[ 0.0 ]])
    expectedResult = self.matrix([[ -0.12738285]])
    result = nn.outputErrorGradient(calculatedOutput, output - calculatedOutput)
    self.assertTrue(np.isclose(result, expectedResult))

  def test_error_gradient2(self):
    dataInput = np.matrix([1, 1], dtype=np.float64)
    activation = nn.SigmoidActivation()
    thresholdsByLayers = []
    weightsByLayers = []
    weightsByLayers.append(self.matrix([[0.5, 0.4], [0.9, 1.0]]))
    weightsByLayers.append(self.matrix([[-1.2, 1.1]]))

    thresholdsByLayers.append(self.matrix([0.8, -0.1]))
    thresholdsByLayers.append(self.matrix([0.3]))
    outputsByLayer =  nn.predictOutputByLayer(dataInput, activation, thresholdsByLayers, weightsByLayers)
    output = self.matrix([[ 0.0 ]])
    outputGradient = nn.outputErrorGradient(outputsByLayer[1], output - outputsByLayer[1])

    expectedResult = self.matrix([[0.03811948, -0.01471182]])
    result = nn.outputErrorGradient(outputsByLayer[0], outputGradient*weightsByLayers[1])


  def test_weights_corrections(self):
    errorGradient =  self.matrix([[ -0.12738285]])
    testInput = self.matrix([0.5250, 0.8808])
    expectedResult = self.matrix([[-0.0066876, -0.01121988]])
    result = nn.calculateWeightCorrections(0.1, testInput, errorGradient)
    
    self.assertTrue(np.isclose(result, expectedResult).all())

  def test_thresholds_corrections(self):
    errorGradient =  self.matrix([[ -0.12738285]])
    expectedResult = self.matrix([[0.01273829]])
    result = nn.calculateThresholdCorrections(0.1, errorGradient)
    self.assertTrue(np.isclose(result, expectedResult))

if __name__ == '__main__':
    unittest.main()