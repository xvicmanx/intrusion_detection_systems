"""
This script is used to test neural network models for all the misuse single attack cases.
Author: Victor Trejo
"""

import os

# Program starting point
if __name__ == "__main__":
	
	currentDirectory = os.path.dirname(os.path.abspath(__file__))
	# Path to read the test datasets
	TEST_DATA_PATH = "{0}/../data/preprocessed/one_vs_all/".format(currentDirectory)
	MODELS_PATH = '{0}/../data/neural_networks_learned_parameters/misuse/one_vs_all/'.format(currentDirectory)
	PYTHON_COMMAND = 'python scripts/neural_networks_evaluation.py'


	for (dirpath, dirnames, filenames) in os.walk(TEST_DATA_PATH):
		for filename in filenames:

			# Only using test data
			if not '.test' in filename: continue

			attackName = filename.replace('misuse_', '').replace('.test', '')
			testile = "{0}{1}".format(TEST_DATA_PATH, filename)
			modelFile = "{0}{1}_trained.pkl".format(MODELS_PATH, attackName)
			numberOfOutputCols = 1
			caseName = 'Misuse {0} attack'.format(attackName)
			
			command = "{0} '{1}' '{2}' '{3}' '{4}'".format(\
				PYTHON_COMMAND,
				testile,
				modelFile,
				numberOfOutputCols,
				caseName
			)

			# Executing command
			result = os.system(command)
			# Break if interrupted
			if result!=0: break



