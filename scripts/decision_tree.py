"""
Decision Tree classifier implementation module.
Author: Victor Trejo
"""
# importing necessary libraries
import math
from collections import Counter
import numpy as np

class Node(object):
	"""Represents the internal nodes of the decision tree"""
	def __init__(self, attribute = None):
		super(Node, self).__init__()
		self.__children = {}
		self.__attribute = attribute
		self.__value = None
		
	def isALeaf(self):
		"""
		Checks wether this node is leaf or not
		Returns : True if it is a leaf node
		"""
		return self.__attribute ==  None

	def addChild(self, key, child):
		"""
		Add child node to the current node.
		parameters: 
				key -> is the key that will be identify the child
				child -> child to be added
		Returns: current node for chaining calls 
		"""
		self.__children[key] = child
		return self

	def getChild(self, key):
		"""
		Returns the child given by the key
		parameters: 
				key -> is the key of the child
		Returns: the child given by the key
		"""
		return self.__children[key]

	def setValue(self, value):
		"""
		Sets the value of the current node
		parameters: 
				value -> value to set to the current node
		Returns: current node for chaining calls 
		"""
		self.value = value
		return self

	def getValue(self):
		"""
		Returns the value of the current node
		Returns: value of the current node
		"""
		return self.value

	def getAttribute(self):
		"""
		Returns the attribute of the current node
		Returns: attribute of the current node
		"""
		return self.__attribute


class DecisionTree(object):
	"""DecisionTree classifier"""
	def __init__(self, root, thresholds):
		super(DecisionTree, self).__init__()
		# root node of the tree
		self.__root = root
		# thresholds for the data sets that are continuous types
		self.thresholds = thresholds
	
	def predict(self, data):	
		"""
		Predicts the label of data sample.
		Parameters: data -> data sample to be predicted.
		Returns: The predicted label.
		"""
		for i in range(len(self.thresholds)):
			# For the continuous type attributes change its value
			# to categorical depending on the threshold value.
			# Values greater than threshold are 1 otherwhise 0
			if not self.thresholds[i] is None:
				data[i] = '0' if float(data[i]) <= self.thresholds[i] else '1'	
		return self.__predictRecursive(data, self.__root)

	def __predictRecursive(self, data, root):
		"""
		Helper function that predicts the label of data sample recursively
		while going through the decision tree nodes.
		Parameters: data -> data sample to be predicted its label.
					root -> root of tree.
		Returns: The predicted label.
		"""
		# leaf nodes store label values, so if the node is a leaf
		# is value its value is returned.
		if root.isALeaf(): 
			return root.getValue()
		else: 
			# Otherwise it is attribute/internal node it
			# should branch depending on the attribute value.
			value = data[root.getAttribute()]
			# Returning the predicted label.
			return self.__predictRecursive(data, root.getChild(value))	



def buildDecisionTree(data, types, attributeValues):
	"""
	From a training data builds a decision tree classifier.
	Parameters: data 			-> training data
				types 			-> types of the attributes values
				attributeValues -> domain of the attributes values
	Returns: A DecisionTree classifier.
	"""
	# data dimensions
	rows, cols = data.shape
	# class labels of the data
	classes = set(list(data[:,cols - 1]))
	# attributes indexes of the data
	attributes = range(cols-1)
	# Preprocessing continuous types attributes values.
	data, thresholds = preprocess(data, attributes, classes, types)	
	# Building decision tree chaining structure.
	root = __buildDecisionTreeRecursively(data, classes, attributes, attributeValues)
	# Creating decision tree using the chaining structure.
	return DecisionTree(root, thresholds)



def __buildDecisionTreeRecursively(data, classes, attributes, attributeUniqueValues):
	"""
	From a training data builds a decision tree node chain structure.
	Parameters: 
				data 				  -> training data
				classes 	    	  -> class labels
				attributes      	  -> set of attributes
				attributeUniqueValues -> domain of the attributes values
	Returns: root of the tree.
	"""
	# data dimensions
	rows, cols = data.shape
	# data output values
	values = list(data[:, cols - 1])
	# Calculating the entropy
	entropy = calculateEntropy(data, classes)
	
	# When all of the examples are of the same class
	# return the value of the class
	if entropy == 0: return Node().setValue(values[0])

	# If there is no more attributes to choose to split
	# return the most frequent class
	maximumValueNode = Node().setValue(max(set(values), key=values.count))
	if len(attributes) <= 0: return maximumValueNode
	
	# Finding best splitting attribute
	attribute = __findBestSplittingAttribute(data, classes, attributes, entropy)
	
	# If No attribute was selected then
	# return the most frequent class
	if attribute is None: return maximumValueNode

	# Removing attribute from the list of attributes to choose
	attributes.remove(attribute)
	
	# # finding the unique values for the attribute
	# attributeUniqueValues = set(data[:,attribute])	
	# Creating root node
	root = Node(attribute)
	# Growing the tree by adding for each attribute value a child node
	for value in attributeUniqueValues[attribute]:
		# Filtering data by attribute value.
		filterData = data[data[:,attribute]==value]
		# Growing the tree recursively
		if filterData.shape[0] > 0:
			child = __buildDecisionTreeRecursively(filterData, classes, attributes, attributeUniqueValues)
		else:
			child = maximumValueNode
		# Adding child node
		root.addChild(value, child)
	return root


def __findBestSplittingAttribute(data, classes, attributes, entropy):
	"""
	Based on informationGain finds the best split attribute to build a tree.
	Parameters:
				data    	-> data set
				classes 	-> class labels
				attributes 	-> data attributes
				entropy     -> entropy of the data.
	Returns: The best splitting attribute
	"""
	# Where the best split attribute will be store
	selectedAttribute = None
	# value of the maximum Information Gain.
	# Initially minus Infinity
	bestInfoGain = float("-inf")
	# For each attribute finding the Information Gain
	# Picking the one that has the biggest Information Gain
	for attributeIndex in attributes:
		# Calculating Information Gain for the current attribute
		counts = Counter(list(data[:,attributeIndex]))
		if len(counts.keys()) == 1: continue

		# infoGain = informationGain(data, attributeIndex, entropy, classes)
		infoGain = calculateGainRatio(data, attributeIndex, entropy, classes)
		# # If the calculated Gain is bigger than the current biggest,
		# update.
		if infoGain > bestInfoGain:
			bestInfoGain = infoGain
			selectedAttribute = attributeIndex
	return selectedAttribute



def changeToCategorical(data, attribute, classes):
	"""
	Changes continuous data to categorical based on the best threshold.
	Parameters:
				data 		-> train data.
				attribute   -> attribute to change to categorical type.
				classes		-> class labels.
	Returns: the data updated with that atrribute change to categorical.
	"""
	values = data[:, attribute].astype(float)
	rows, cols = data.shape
	newData = data.copy()
	newData = newData[newData[:,attribute].argsort()]
	entropy = calculateEntropy(data, classes)
	counts = Counter(list(data[:,attribute]))
	
	bestInfoGain = float("-inf")
	threshold = None
	if len(counts.keys()) == 1:
		threshold = float(counts.keys()[0])
	else:
		for i in range(rows - 1):
			value = (float(newData[i, attribute]) + float(newData[i+1,attribute]) )/2
			if newData[i, cols - 1] != newData[i+1, cols - 1]:
				copy = data.copy()
				# For the continuous type attributes change its value
				# to categorical depending on the threshold value.
				# Values greater than threshold are 1 otherwhise 0
				copy[values > value,attribute] = 1
				copy[values <= value, attribute] = 0
				# Calculating attribute's information gain	
				infoGain = informationGain(copy, attribute, entropy, classes)
				
				# infoGain = calculateGainRatio(copy, attribute, entropy, classes)
				if infoGain > bestInfoGain:
					bestInfoGain = infoGain
					threshold = value
	# For the continuous type attributes change its value
	# to categorical depending on the threshold value.
	# Values greater than threshold are 1 otherwhise 0
	data[values > threshold,attribute] = 1
	data[values <= threshold, attribute] = 0	
	# returnin data and threshols
	return data, threshold


def preprocess(data, attributes, classes, types):
	"""
	Preprocesses the training data to change the continuous attributes to categorical.
	Parameters: 
				data 		-> training data
				attributes 	-> data attributes
				classes		-> data class labels
				types		-> types of the attributes
	Returns: the preprocessed data.
	"""
	rows, cols = data.shape
	classes = set(list(data[:,cols - 1]))
	attributes = range(cols-1)
	thresholds = []
	for attribute in attributes:
		threshold = None
		if types[attribute] == "Continuous":
			data,threshold = changeToCategorical(data, attribute, classes)
		thresholds.append(threshold)
	return data, thresholds 


def calculateEntropy(data, classes):
	"""
	Calculates the entropy of a the training data.
	Parameters: 
				data  	-> training data.
				classes	-> class labels.
	Returns: the calculated entropy
	"""
	result = 0.0
	rows, cols = data.shape
	counts = Counter(list(data[:,cols-1]))
	for classValue in classes:
		fraction = float(counts[classValue])/rows
		result-= 0 if fraction == 0 else  fraction * math.log(fraction, 2)
	return result


def informationGain(data, attribute, entropy, classes):
	"""
	Calculates the information gain of an attribute.
	Parameters: 
				data 		-> training data.
				attribute   -> attribute.
				entropy     -> entropy of the data.
				classes		-> class labels of the data.
	Returns: the calculated Information Gain.
	"""
	rows, cols = data.shape
	counts = Counter(list(data[:,attribute]))
	uniqueValues = set(data[:,attribute])

	# Calculating information Gain for current attribute
	infoGain = entropy
	for value in uniqueValues:
		filterData = data[ data[:,attribute] == value ]
		fraction = float(counts[value])/rows
		infoGain-= fraction * calculateEntropy(filterData, classes)
	return infoGain

def calculateGainRatio(data, attribute, entropy, classes):
	"""
	Calculates the gain ratio of an attribute.
	Parameters: 
				data 		-> training data.
				attribute   -> attribute.
				entropy     -> entropy of the data.
				classes		-> class labels of the data.
	Returns: the calculated Gain ratio.
	"""
	infoGain = informationGain(data, attribute, entropy, classes)
	splitInfo = calculateSplitInfo(data, attribute)
	ratio = infoGain/splitInfo
	return ratio


def calculateSplitInfo(data, attribute):
	"""
	Calculates the splits info.
	Parameters: 
				data 		-> training data.
				attribute   -> attribute.
	Returns: the calculated split info
	"""
	rows, cols = data.shape
	counts = Counter(list(data[:,attribute]))
	uniqueValues = set(data[:,attribute])

	# Calculating the split info
	splitInfo = 0.0
	for value in uniqueValues:
		fraction = float(counts[value])/rows
		splitInfo -= 0 if fraction == 0 else  fraction * math.log(fraction, 2)
	
	# if splitInfo == 0: splitInfo = 1
	return rows*splitInfo