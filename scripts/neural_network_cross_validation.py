"""
This scripts is used to make cross validation in terms of the number of neurons for the neural network.
Author: Victor Trejo
"""
import os
import numpy as np
import my_utils as mu
import sys
import neural_networks as nn
import random

def calculateAccuracy(model, testInput, testOutput):
	"""
	Calculates the acurracy of a model on a test data
	parameters:	
				testInput  -> features of the test data
				testOutput -> output of the test data
	Returns: the value of the calculated acurracy 
	"""
	# Data dimensions
	rows, cols = testInput.shape
	correctlyPredicted = 0

	# For every row/sample in the data:
	# predict and evaluate.
	for i in range(rows):
		# Saving the real label of the data
		real = int(round(testOutput[i,:]))
		# predicting the label
		predicted = int(round(model.predict(testInput[i,:])))
		# If the label was correctly predicted
		# then increase the number of correctly predicted label
		if predicted == real:
			correctlyPredicted+=1
	return float(correctlyPredicted)/rows


def kFoldCrossValidation(dataInput, dataOutput, neuronsByLayers, numberOfFolds):
	"""
	Performs k fold cross validation to evaluate the model for the given neurons by layer.
	parameters:
				dataInput  		-> features of the data
				dataOutput 		-> output values of the data
				neuronsByLayers -> number of neurons by layer
				numberOfFolds   ->	number of folds to make
	Returns:   The average of the accuracy values
	"""
	inputRows, inputCols = dataInput.shape
	outputRows, outputCols = dataOutput.shape
	acurracyValues = []
	activation = nn.SigmoidActivation()

	foldSize = inputRows/numberOfFolds

	# Separating the data into folds
	foldValues = []
	for i in range(numberOfFolds):
		foldDataInput = dataInput[i*foldSize:(i+1)*foldSize,:]
		foldDataOutput = dataOutput[i*foldSize:(i+1)*foldSize,:]
		foldValues.append((foldDataInput, foldDataOutput))

	# Use every fold as a test and the remaining as training set.
	for i in range(numberOfFolds):
		testDataInput = foldValues[i][0]
		testDataOutput = foldValues[i][1]

		trainDataInput = None
		trainDataOutput = None
		for j in range(numberOfFolds):
			
			if i!=j:
				if not trainDataInput is None:
					trainDataInput = np.concatenate((trainDataInput, foldValues[j][0]), axis=0)
					trainDataOutput = np.concatenate((trainDataOutput, foldValues[j][1]), axis=0)
				else:
					trainDataInput = foldValues[j][0]
					trainDataOutput = foldValues[j][1]
					
		# Training model from data
		print "Training"
		weightsByLayers, thresholdsByLayers, errors, learningRates = nn.train(\
			trainDataInput,
			trainDataOutput,
			activation,
			0.1,
			neuronsByLayers
		)

		print "Testing"
		# building model
		model = nn.NeuralNetwork(\
			weightsByLayers,
			thresholdsByLayers,
			activation
		)

		accuracy = calculateAccuracy(model, testDataInput, testDataOutput)
		print accuracy
		acurracyValues.append(accuracy)

	return sum(acurracyValues)/numberOfFolds


def evaluateNeuronsNumbers(dataInput, dataOutput, debug):
	"""
	Calculates the average accuracy accuracy of each model neurons size
	parameters:
				dataInput  -> features of the data
				dataOutput -> output values of the data
				debug      -> flag to indicate if there will be debugging or not.
	"""
	nn.PRINT_SQUARED_ERRORS = debug
	outputRows, outputCols = dataOutput.shape
	# Evaluating different neurons sizes
	innerLayerNeurons = 2
	while innerLayerNeurons < 20:
		print "Evaluating {}".format(innerLayerNeurons) 
		averageAccuracy = kFoldCrossValidation(dataInput, dataOutput, (innerLayerNeurons, outputCols), 5)
		print averageAccuracy
		innerLayerNeurons*=2


	


# Program starting point
if __name__ == "__main__":
	if len(sys.argv) < 3: 
		print "Invalid number of arguments!"
		print "Please provide 'dataFilePath' 'number of columns in the output'"
	else: 
		# Reading the file names from the parameters passed from the console
		dataFilePath = sys.argv[1]
		outputCols = int(sys.argv[2])
		debug = 'debug' in sys.argv
		# Reading training data file
		data =  mu.readDataFile(dataFilePath, 1)
		random.shuffle(data)
		data = np.matrix(data, dtype=np.float64)
		rows, cols = data.shape

		# Separating attributes(input) from the classes(output)
		dataInput, dataOutput = data[:,0:cols-1], data[:,-outputCols:]
		evaluateNeuronsNumbers(dataInput, dataOutput, debug)