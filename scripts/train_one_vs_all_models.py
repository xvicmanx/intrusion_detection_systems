"""
This script is used to train neural network models for all the misuse single attack cases.
Author: Victor Trejo
"""

import os
import sys

# Program starting point
if __name__ == "__main__":
	
	currentDirectory = os.path.dirname(os.path.abspath(__file__))
	# Path to read the training datasets
	TRAIN_DATA_PATH = "{0}/../data/preprocessed/one_vs_all/".format(currentDirectory)
	MODELS_PATH = '{0}/../data/neural_networks_learned_parameters/misuse/one_vs_all/'.format(currentDirectory)
	PYTHON_COMMAND = 'python scripts/train_neural_network_model.py'

	# These are flags for debugging the training purposes
	debug = 'debug' in sys.argv
	plot = 'plot' in sys.argv

	for (dirpath, dirnames, filenames) in os.walk(TRAIN_DATA_PATH):
		for filename in filenames:
			
			# Only using train data
			if not '.train' in filename: continue
			
			attackName = filename.replace('misuse_', '').replace('.train', '')
			# Where the train data is being obtained
			trainFile = "{0}{1}".format(TRAIN_DATA_PATH, filename)
			# Where the model will be stored
			modelFile = "{0}{1}_trained.pkl".format(MODELS_PATH, attackName)

			# The neurons by layer: 2 in the hidden and 1 in the output.
			neuronsByLayer = '2,1'
			learningRate = 0.1
			numberOfOutputCols = 1

			caseName = 'Misuse {0} attack'.format(attackName)

			# Preparing command
			command = "{0} '{1}' '{2}' '{3}' '{4}' '{5}' '{6}' '{7}' '{8}'".format(\
				PYTHON_COMMAND,
				trainFile,
				modelFile,
				learningRate,
				neuronsByLayer,
				numberOfOutputCols,
				caseName,
				"debug" if debug else '',
				"plot"  if plot else ''
			)

			# Executing command
			result = os.system(command)

			# Break if interrupted
			if result!=0: break



