"""
This script is used to train neural networks model from data.
Author: Victor Trejo
"""
import os
import numpy as np
import my_utils as mu
import sys
import neural_networks as nn
import pickle
import matplotlib.pyplot as plt


def trainAndStore(\
	dataInput,
	dataOutput,
	activation,
	learningRate,
	neuronsByLayers,
	outputFilePath,
	debug
):
	"""
	Given input and output data, the learning rate,
	neurons by layers and outputFile, trains a neural networks
	calculating the weights and thresholds by layers to stored in the specified file.
	Parameters:
				dataInput -> train input data
				dataInput -> train output data
				activation -> activation function
				learningRate -> learning rate of the neural networks
				neuronsByLayers -> a tuple that especifies the number of neurons by layer
				outputFilePath -> the file where the learned parameters will be stored.
				debug          -> For debugging purposes
	"""
	nn.PRINT_SQUARED_ERRORS = debug

	# Training model from data
	weightsByLayers, thresholdsByLayers, squaredErrors, learningRateValues = nn.train(\
		dataInput,
		dataOutput,
		activation,
		learningRate,
		neuronsByLayers
	)

	# Formatting the data before it is stored
	trainedData = {\
		'weights': weightsByLayers,
		'thresholds': thresholdsByLayers
	}

	# Saving the learned parameters 
	outputFile = open(outputFilePath, 'wb')
	pickle.dump(trainedData, outputFile)
	outputFile.close()
	return squaredErrors, learningRateValues


# Program starting point
if __name__ == "__main__":
	if len(sys.argv) < 6: 
		print "Invalid number of arguments!"
		print "Please provide 'data file path' 'output file path' 'learning rate' 'neurons by layers' 'number of colums in the output'"
	else: 
		# Reading the file names from the parameters passed from the console
		dataFilePath = sys.argv[1]
		outputFilePath = sys.argv[2]
		learningRate = float(sys.argv[3])
		neuronsByLayers = tuple(map(lambda x: int(x), sys.argv[4].split(",")))
		outputCols = int(sys.argv[5])
		caseName = sys.argv[6]
		debug = 'debug' in sys.argv
		shouldPlot = 'plot' in sys.argv

		# Reading training data file
		data =  mu.readDataFile(dataFilePath, 1)
		data = np.matrix(data, dtype=np.float64)
		rows, cols = data.shape
		# Separating attributes(input) from the classes(output)
		dataInput, dataOutput = data[:,0:cols-outputCols], data[:,-outputCols:]

		print "Training {0} Started\n".format(caseName)
		squaredErrors, learningRateValues = mu.measureRunningTime(lambda: trainAndStore(\
										dataInput,
										dataOutput,
										nn.SigmoidActivation(),
										learningRate,
										neuronsByLayers,
										outputFilePath,
										debug
										)
		)
		print "\nThe memory usage was: {} MB\n".format(mu.getMemoryUsage())
		print "Training {0} Finished\n".format(caseName)
		print "Trained data stored in '{}'\n".format(outputFilePath)


		if shouldPlot:
			# Displaying how the learning rate by epoch
			
			plt.subplot(211)
			plt.title("Learning rate by epoch")
			plt.plot(learningRateValues)
			plt.ylabel('Learning rate value')
			plt.xlabel('Epochs')
			# Displaying how the squared error decreased by epoch
			plt.subplot(212)
			plt.plot(squaredErrors)
			plt.title('Sum Squared Error by epoch')
			plt.ylabel("Sum Squared Error value")
			plt.xlabel('Epochs')
			plt.show()