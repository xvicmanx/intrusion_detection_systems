"""
This script is used to test neural network models for all the anomaly system cases.
Author: Victor Trejo
"""

import os

# Program starting point
if __name__ == "__main__":
	
	currentDirectory = os.path.dirname(os.path.abspath(__file__))
	# Path to read the test datasets
	TEST_DATA_PATH = "{0}/../data/preprocessed/".format(currentDirectory)
	MODELS_PATH = '{0}/../data/neural_networks_learned_parameters/'.format(currentDirectory)
	
	command = "{0} '{1}' '{2}' '{3}' '{4}'".format(\
		'python scripts/neural_networks_evaluation.py',
		"{0}{1}".format(TEST_DATA_PATH, 'anomaly_binary_output.test'),
		"{0}anomaly_trained.pkl".format(MODELS_PATH),
		1,
		'Anomaly system'
	)

	# Executing command
	os.system(command)




